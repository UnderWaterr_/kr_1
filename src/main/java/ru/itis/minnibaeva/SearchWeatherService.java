package ru.itis.minnibaeva;

import java.io.IOException;

public interface SearchWeatherService {
    String getWeatherJSON(String url);
    String parseGson(StringBuilder jsonString) throws IOException;
}
